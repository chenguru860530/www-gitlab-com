---
layout: markdown_page
title: "Account Onboarding"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

- Account Onboarding *(Current)*
- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [Account Triage](handbook/customer-success/tam/triage)
- [Account Engagement](/handbook/customer-success/tam/engagement)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Gemstones](/handbook/customer-success/tam/gemstones)
- [Escalation Process] (/handbook/customer-success/tam/escalations/)

---

# Customer Onboarding

When a customer with the Gemstone Ruby or above purchases or upgrades GitLab, an [onboarding object](/handbook/customer-success/using-salesforce-within-customer-success/#onboarding-objects) is automatically created.

The Technical Account Management team are subscribed to a Customer Onboarding Report in Salesforce which is sent every Monday. If new accounts have joined, the Technical Account Management Leader should align the account with a Technical Account Manager and kick off the customer's onboarding process. Due to the nature of Salesforce automations, and this being an early iteration, it is up to the TAM Leader to regularly clean up this report by deleting any duplicates. 

_Handy tip: In order to delete an onboarding object, you must first make yourself the owner of it._

If an account moves into a higher Gemstone category from a lower category, the same process should be followed. AKA Ruby or Sapphire to Pearl or Diamond. These are _usually_ significant enough upgrades to warrant a dedicated Technical Account Manager and a fresh onboarding experience.

It doesn't matter who the Onboarding object owner is as long as the Technical Account Manager field is completed on the object. The object owner will usually be auto-generated and doesn't mean anything.

Once an account has been assigned to a Technical Account Manager, they must open an onboarding issue in the [Customer Onboarding GitLab project](https://gitlab.com/gitlab-com/customer-success/customer-onboarding). When you open a new issue in the project, you will be presented with instructions. 

This project is internal only (not customer-facing) and is to help and guide Technical Account Managers in working through their customer onboarding properly with each customer. It also provides a platform for communication outside of Salesforce to pull in other members of the GitLab team for discussion if needed. You must complete all tasks within the issue in order to close it, and the onboarding object in Salesforce cannot be closed until this issue is completed. Do not skip steps. If you need help, please ask your leader and your team for support and guidance.

The onboarding object must be completed in parrallel to the issue as Salesforce is our single source of truth and the fields in the object can be reported upon for historical context and metrics.

The onboarding object status and date time-box must be updated as quickly as possible upon account assignment. This is important for reporting purposes. 

In order to complete the onboarding object, the onboarding survey _must_ be sent to the customer. Be sure to send a personalised email to the team of people in the customer's organization that you have been working with throughout the onboarding time-box: [Here is the link to the survey](https://goo.gl/forms/aYOLCCkdM52yfaaD3).

When the onboarding is complete, the Technical Account Manager will either continue to work with the customer permanently if they are so aligned or the account will be transitioned to a new Technical Account Manager if the previous Technical Account Manager was just stepping in to provide onboarding support to our customer base. It is important that all of our customers are properly onboarded in order to improve retention and ensure the best customer experience possible. 