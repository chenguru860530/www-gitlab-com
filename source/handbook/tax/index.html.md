---
layout: markdown_page
title: "The GitLab Tax Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}
 
# Keep calm and love taxes!

## Contacting the Tax Team

The tax department is responsible for GitLab’s overall tax strategy including all components of tax compliance, tax planning and accounting for income taxes. Tax regulations taxation differ between countries, which can make this area complex. The tax team is here to support you, make it simple for you and guide you through the landscape of taxes. In case you have any ad-hoc questions please feel free reach out on the #tax channel on Slack. For the sake of clarity please do not use it to seek tax advice for personal matters. We will try to to our best to answer your questions on taxation of your #stock options though. For any in-depth discussions please reach out to the team.

## Corporate Structure

Please click this link for our [Corporate Structure](https://docs.google.com/document/d/1BZT9nnZEmxDRer5qF0P9C4U4S0-PlXzVkrkG9rSk9gE/edit?usp=sharing)